#! /usr/bin/python3
# -*- coding:Utf8 -*-
"""
this is script.py that replace world with webforce in hello.txt 
"""
def appli():
    "appli opens hello.txt and replace 'world' with 'webforce'"
    with open('hello.txt','r',encoding='utf-8') as fileo:
        txt=fileo.read()
        txt=txt.replace('world','webforce')
    with open ('hello.txt','w',encoding='utf-8') as fileo:
        fileo.write(txt)

if __name__ == '__main__':
    appli()
