# build:
docker build . -f CorrigeExercice_Dockerfile -t img_name

# run without closing
docker run -d --name container_name img_name:latest sleep 70000

# enter container and check manually hello.txt file
docker exec -it container_name bash
cat hello.txt 

>expected ouput : hello webforce
