#!/bin/bash

###############################################################
#  DESCRIPTION:  création d'un user spécific pour docker
###############################################################


groupadd -g 500000 dockremap && 
groupadd -g 501000 dockremap-user && 
useradd -u 500000 -g dockremap -s /bin/false dockremap && 
useradd -u 501000 -g dockremap-user -s /bin/false dockremap-user

echo "dockremap:500000:65536" >> /etc/subuid && 
echo "dockremap:500000:65536" >>/etc/subgid
#default utilise dockremap comme utilisateur par defaut, cf docs ici : 
#https://docs.docker.com/engine/security/userns-remap/#enable-userns-remap-on-the-daemon
echo "
  {
   \"userns-remap\": \"default\"
  }
" > /etc/docker/daemon.json

systemctl daemon-reload && systemctl restart docker
