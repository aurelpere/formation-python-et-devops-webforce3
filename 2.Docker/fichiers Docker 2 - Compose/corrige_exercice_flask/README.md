# appli de sondage nginx et flask

Lancer les container avec 
docker-compose -f docker-compose-nginx.yml up -d
ou
docker-compose -f docker-compose-traefik.yml up -d


# verification manuelle que nginx fonctionne (docker-compose-nginx.yml)

verifier que le serveur nginx tourne sur :

http://localhost/blabla/ pour les "mauvaises" requetes

et 

http://localhost/blabla/?answeryes=1&answerno=0 pour voter oui
http://localhost/blabla/?answeryes=0&answerno=1 pour voter non

et 

http://localhost/blabla/results.png pour les graphiques de résultats

# verification que traefic fonctionne (docker-compose-traefik.yml)

Vous pouvez en principe remplacer localhost par sondage.localhost dans les adresses précédentes pour vérifier que traefik fonctionne. 


Traefik et nginx proposent tous les deux des fonctions de reverse proxy et on trouve le plus souvent de la literature sur l'utilisation de l'un ou l'autre mais rarement des deux en même temps.

