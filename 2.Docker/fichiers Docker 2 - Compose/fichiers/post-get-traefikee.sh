#!/bin/bash
echo "Contenu de la base redis avant POST"
curl yourname.localhost:80

echo "POST..."
curl --header "Content-Type: application/json" --request POST --data '{"name":"Ada_Lovelace"}' yourname.localhost:80

echo "Contenu de la base redis après POST"
curl yourname.localhost:80

