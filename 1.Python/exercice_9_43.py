#! /usr/bin/python3
# -*- coding:Utf-8 -*-
#exercice 10.35
#Créez une liste A contenant quelques éléments. Effectuez une vraie copie de cette liste dans une
#nouvelle variable B. Suggestion : créez d’abord une liste B de même taille que A mais ne
#contenant que des zéros. Remplacez ensuite tous ces zéros par les éléments tirés de A.


def copieListe(liste1):
    "la fonction copieListe(liste1) copie la liste 1 dans la liste 2"
    liste2=[0]*len(liste1)    
    for i in range(len(liste1)):
        liste2[i]=liste1[i]
    return liste2 

#test
A=[1,"la bite à urbain", 3, 'ceci est une liste A']
print(A)
a=copieListe(A)
print (a)
a[1]='merde'

print('petit test de changement sur a pour voir le linkage')
print(A)
print(a)



