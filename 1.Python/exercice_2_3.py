#3. Écrivez un programme qui affiche une suite de 12 nombres dont chaque terme soit égal au triple du terme précédent.
old=1#on initialise (on affecte une valeur à une variable qu'on va incrémenter par la suite) une variable old à 1
terme=1 #on initialise (idem) une variable terme à 1
while old<12:#tant que old est inférieur à 12 la boucle while va tourner. elle s'arretera quand old est égal à 12 puisqu'on l'incrémente ci-dessous
    terme=terme*3#on affecte à la variable terme la valeur de la variable terme multipliée par 3
    old+=1#on affecte à la variable old la valeur de la variable old ajouté de 1
    print(terme)#on imprime la variable terme
