#14.Écrivez un programme qui analyse un par un tous les éléments d’une liste
#de nombres (par exemple celle de l’exercice précédent) pour générer deux
#nouvelles listes. L’une contiendra seulement les nombres pairs de la liste
#initiale, et l’autre les nombres impairs. Par exemple, si la liste initiale est celle
#de l’exercice précédent, le programme devra construire une liste pairs qui
#contiendra [32, 12, 8, 2], et une liste impairs qui contiendra [5, 3, 75, 15].
#Astuce : pensez à utiliser l’opérateur modulo (%) déjà cité précédemment.
list1=[32, 5, 12, 8, 3, 75, 2,15]
pairlist=[]
impairlist=[]
def pair_define(val):
    pairlist.append(val)
def impair_define(val):
    impairlist.append(val)
definedict={1:pair_define,0:impair_define}
for index,val in enumerate(list1):
    definedict[val%2==0](val)
#ou    definedict[val//2>0](val)
print('liste paire',pairlist)
print('liste impaire',impairlist)
#ou
pairlist=[]
impairlist=[]
for index,val in enumerate(list1):
    if val%2==0:
        pairlist.append(val)
    else:
        impairlist.append(val)
print('liste paire ', pairlist)
print('liste impaire ', impairlist)
