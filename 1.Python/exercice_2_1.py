#1. Ecrivez un programme qui affiche les 20 premiers termes de la table de multiplication par 7.
index=1 #on initialise la variable index
while index<21: #boucle while qui tourne tant que index est inferieur à 21 et qui s'arrête quand index==21
    print(index*7) #on imprime la valeur de la variable index (un entier) multipliée par 7
    index=index+1#on itère : on donne à la variable index la valeur index+1
