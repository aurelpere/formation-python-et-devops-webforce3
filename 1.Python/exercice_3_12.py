#12.Soient les listes suivantes :
#t1 = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
#t2 = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin',
#'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre']
#Écrivez un petit programme qui crée une nouvelle liste t3. Celle-ci devra
#contenir tous les éléments des deux listes en les alternant, de telle manière que
#chaque nom de mois soit suivi du nombre de jours correspondant :
#['Janvier',31,'Février',28,'Mars',31, etc...].

#documentation itertools
from itertools import cycle,islice
def roundrobin(*iterables):
    "roundrobin ('ABC','D','EF') --> A D E B F C"
    num_active=len(iterables)
    nexts=cycle(iter(it).__next__ for it in iterables)
    while num_active:
        try:
            for next in nexts:
                yield next()
        except StopIteration:
            #Remove the iterator we just exhausted from the cycle
            num_active=1
            nexts=cycle(islice(nexts,num_active))
#ou :

list1=[ 31,
        28,
        31,
        30,
        31,
        30,
        31,
        31,
        30,
        31,
        30,
        31]
list2=['janvier',
        'fevrier',
        'mars',
        'avril',
        'mai',
        'juin',
        'juilet',
        'aout',
        'septembre',
        'octobre',
        'novembre',
        'decembre']
list3=[]
for idx,value in enumerate(list1):
    list3.append(list1[idx])
    list3.append(list2[idx])
print(list3)


