#! /usr/bin/python3
# -*- coding:Utf-8 -*-
class Rectangle(object):
    "Classe de rectangles"
    def __init__(self, longueur =0, largeur =0):
        self.L = longueur
        self.l = largeur
        self.nom ="rectangle"
    def perimetre(self):
        return f"({self.L} + {self.l}) * 2 = {(self.L+self.l)*2}"
    def surface(self):
        return f"{self.L} * {self.l} = {self.L*self.l}"
    def mesures(self):
        print(f"Un {self.nom} de {self.L} sur {self.l}")
        print(f"a une surface de {self.surface()}")
        print(f"et un périmètre de {self.perimetre()}\n")
 
class Carre(Rectangle):
    "Classe de carrés"
    def __init__(self, cote):
        Rectangle.__init__(self, cote, cote)
        self.nom ="carré"

if __name__ == "__main__":
    r1 = Rectangle(15, 30)
    r1.mesures()
    c1 = Carre(13)
    c1.mesures()
