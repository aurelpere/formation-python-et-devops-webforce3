#7.Écrivez un programme qui convertisse en degrés Celsius une température exprimée au départ en degrés Fahrenheit, ou l’inverse.
#La formule de conversion est : T F =T C ×1,8+32

print('entrer votre valeur en degré celcius')
tc=input() #demande à l'utilisateur d'entrer une valeur qui sera de type string (chaine de caractère)
tf=int(tc)*1.8+32 #ici int(tc) convertit la variable tc en integer
print(f'la temp en defré farenheit est {tf}')#ici on utilise une f-string avec {} qui permet d'utiliser la variable tf et de l'intégrer à la chaine de caractère
