#affichage des 20 premiers termes de la table de 7 avec * sur les multiples de 3
index=1#on initialise la variable index (on affecte à la variable index qu on va incrémenter par la suite la valeur 1 )
mult=7#on affecte à la variable mult la valeur 7. mult sera multiplié par index pour trouver le résultat
result=0#on initialise la variable index à la valeur 0 qu'on va modifier dans la boucle while pour obtenir les résultats voulus
while index<21:#on fait une boucle while sur la variable index : tant qu'elle est inférieure à 21, la boucle va tourner
    result=index*mult#on affecte à la variable result la valeur de index(1 à la premiere itération et +n à la nième itération suivante) multiplié par mult(7) 
    if result%3==0:#si le reste de la division de result par 3 est égal à 0 (cad si result est un mutliple de 3)
        print(result,'*')#printer la variable result avec une chaine étoile concaténée
    else:#sinon
        print(result)#printer la variable result
    index+=1#on affecte à la variable index la valeur de index+1

