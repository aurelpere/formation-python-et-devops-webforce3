#19. Définissez une fonction surfCercle(R). Cette fonction doit renvoyer la surface (l’aire) d’un cercle dont on lui a fourni le rayon R en argument. Par exemple, l’exécution de #l’instruction : print(surfCercle(2.5)) doit donner le résultat : 19.63495...
from math import pi
def surfCercle(R):
    surf=pi*R**2
    print(surf)
surfCercle(2.5)

