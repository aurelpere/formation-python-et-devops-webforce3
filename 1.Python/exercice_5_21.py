#21. Définissez une fonction compteCar(ca,ch) qui renvoie le nombre de fois que 
#l’on rencontre le caractère ca dans la chaîne de caractères ch. Par exemple, 
#l’exécution de l’instruction : print(compteCar(’e’, ’Cette phrase est un exemple’)) 
#doit donner le résultat : 7

def compteCar(ca,ch):
    compteur=0
    for car in ch:
        if car==ca:
            compteur+=1
    return compteur
print(compteCar('e','Cette phrase est un exemple'))
#ou

def compteCar(ca,ch):
    def iterateur(compteur):
        return compteur+1
    def iso(compteur):
        return compteur
    dict={1:iterateur,0:iso}
    compteur=0
    for car in ch:
        compteur=dict[car==ca](compteur)
    return compteur
print(compteCar('e','Cette phrase est un exemple'))
