#23. Définissez une fonction changeCar(ch,ca1,ca2,debut,fin) qui remplace tous les 
#caractères ca1 par des caractères ca2 dans la chaîne de caractères ch, à partir de 
#l’indice debut et jusqu’à l’indice fin, ces deux derniers arguments pouvant être omis (et 
#dans ce cas la chaîne est traitée d’une extrémité à l’autre). Exemples de la fonctionnalité 
#attendue :
#>>> phrase = ' Ceci est une toute petite phrase. '
#>>> print(changeCar(phrase, ' ' , '*' ))
#Ceci*est*une*toute*petite*phrase.
#>>> print(changeCar(phrase, ' ' , '*' , 8, 12))
#Ceci est*une*toute petite phrase.
#>>> print(changeCar(phrase, ' ' , ' * ' , 12))
#Ceci est une*toute*petite*phrase.
#>>> print(changeCar(phrase, ' ' , ' * ' , fin = 12))
#Ceci*est*une*toute petite phrase.

def changeCar(ch,ca1,ca2,debut=0,fin=0):
    if fin==0:
        fin=len(ch)-1
    chaine=''
    i=0
    while i<debut:
        chaine+=ch[i]
        i+=1
    while i>=debut and i<=fin:
        if ch[i]==ca1:
            chaine+=ca2
            i+=1
        else:     
            chaine+=ch[i]
            i+=1
    while i>fin and i<len(ch):
        chaine+=ch[i]
        i+=1
    return chaine
phrase='Ceci est une toute petite phrase.'
print(changeCar(phrase," ",'*'))
print(changeCar(phrase," ","*",8,12))
print(changeCar(phrase," ","*",12))
print(changeCar(phrase," ","*",fin=12))
