#4. Écrivez un programme qui convertit un nombre entier de secondes fourni au départ en un nombre d’années, de mois, de jours, de #minutes et de secondes (utilisez l’opérateur division entière // et l’opérateur modulo : % ).
#7//3 donne le quotient de la division de 7 par 3, cad donne 2
#6%3 donne le reste de la division de 6 par 3, cad donne 0

print("entrer le nombre de secondes") #on imprime une phrase invitant l'utilisateur a entrer le nombre de secondes. python utilise de maniere indifférenciée le simple guillement ' ou " pour les chaines de caractères. 

nbs=input()#input permet à l'utilisateur d'entrer une valeur. on affecte à la variable nbs la valeur entrée
nbs=int(nbs)#on affecte à la variable nbs la valeur de nbs convertie en entier avec int(nbs) car la valeur de input est de type chaine donc la variable nbs est de type chaine

ns_an=12*31*24*60*60#la variable nbs_an est le nb de seconde dans une année
#si on a un doute sur ses librairies C:
#assert ns_an=32140800
ns_mois=31*24*60*60#la variable nbs_mois est le nb de secondes dans un mois de 31j(on simplifie la durée d'un mois)
#si on a un doute sur ses librairies C:
#assert ns_mois=2678400
ns_jour=24*60*60#la variable nbs_jour est le nb de secondes dans un jour
#si on a un doute sur ses librairies C:
#assert ns_jour=86400
ns_min=60#la variable nbs_min est le nb de secondes dans une minute 

result_an=nbs//nbs_an #la variable result_an est le quotient de la division du nombre de secondes entrées par l'utilsateur (la variable nbs) par le nombre de secondes dans un an (la variable nbs_an)
rest_an=nbs%nbs_an #la variable rest_an est le reste de la division du nombre de secondes entrées par l'utilisateur (idem) par le nombre de secondes dans un an(idem)

print(rest_an)

result_mois=rest_an//nbs_mois#la variable result_mois est le quotient de la division du nombre de secondes de rest_an par le nombre de secondes dans un mois (la variable nbs_mois)
rest_mois=rest_an%nbs_mois#la variable rest_mois est le reste de la division du nombre de secondes de rest_an par le nombre de secondes dans un mois (la variable nbs_mois)

result_jours=rest_mois//nbs_jour#la variable result_jours est le quotient de la division du nombre de secondes de rest_mois par le nombre de secondes dans un jour (la variable nbs_jour)
rest_jours=rest_mois%nbs_jour#la variable rest_jours est le reste de la division du nombre de secondes de rest_mois par le nombre de secondes dans un jour (la variable nbs_jour)

result_min=rest_jours//nbs_min#la variable result_min est le quotient de la division du nombre de secondes de rest_jours par le nombre de secondes dans une minute (la variable nbs_min)
rest_min=rest_jours%nbs_min#la variable rest_min est le reste de la division du nombre de secondes de rest_jours par le nombre de secondes dans une minute (la variable nbs_min)

result_s=rest_min#le nombre de secondes qui reste est le nombre de minute de rest_min multipliée par le nombre de secondes dans une minute (nbs_min)

print(f'{result_an} ','années',f'{result_mois} ','mois',f'{result_jours} ','jours',f'{result_min} ','minutes',f'{result_s} ','secondes')
#test
