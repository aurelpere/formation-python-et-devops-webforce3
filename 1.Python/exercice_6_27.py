#! /usr/bin/python3
# -*- coding:Utf8 -*-
import os
def change_word(fichier1,fichier2,mot_1,mot_2):
    "change le mot_1 par mot_2 dans le texte de fichier1 et le copie dans fichier2"
    with open (fichier1,'r',encoding='utf-8') as fileo:
        txt=fileo.read()
        txt=txt.replace(mot_1,mot_2)
    with open (fichier2,'w',encoding='utf-8') as fileo:
        fileo.write(txt)

# test :
if __name__ == "__main__":
    with open ('test','w',encoding='utf-8') as fileo:
        fileo.write('ceci est un test')
    change_word('test','test_reussi',mot_1='test',mot_2='test reussi')
    with open ('test_reussi','r',encoding='utf-8') as fileo:
        txt=fileo.read()
    print(txt)
    os.remove('test')
    os.remove('test_reussi')
