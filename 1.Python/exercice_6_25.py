#! /usr/bin/python3
# -*- coding:Utf8 -*-
#script python 
# Considérons que vous avez à votre disposition un fichier texte contenant des phrases de
#différentes longueurs. Écrivez un script qui recherche et affiche la phrase la plus longue.
#fonctions

#  /!\ Attention la fonction max(x) ne renvoie pas la chaine de caractères la plus longue 
# pour une liste dans le cas où la chaîne la plus longue contient '\ ou des caractères
# spéciaux qui faussent l'approximation max(x)
# Il faudrait modifier le script pour utiliser len (liste[element]) et enlever ensuite les \

def existe(x):
    "fonction existe(x) qui retourne 0 si le fichier x existe et 0 s'il n'existe pas"
    try:
        f=open(str(x),'r')
        f.close()
    except:
        return 0
    else:
        return 1

def demanderfichier():
    "fonction demanderfichier() qui renvoie la chaine de caractères entrées"
    print ('quel est le nom du fichier à ouvrir?')
    x=input()
    return str(x)
    

def demanderquoifaire():
    "fonction demanderquoifaire() qui renvoie le caractère 'r' ou 'a'"
    print ('que voulez vous faire? r pour afficher le contenu et a pour enregistrer de nouvelles lignes')
    var=input()
    if var!='r' and var!='a':
        demanderquoifaire()
    else:
        return var 
#__main__
filename=demanderfichier()
while 1:
    if existe (filename):
        print ("nous allons rechercher la phrase la plus longue")
        break
    else:
        print(f"Le fichier {filename} est introuvable")
        filename=demanderfichier()

Monfichierobjet=open('Monfichier','r')
x=Monfichierobjet.readlines()
print(x)
phraselapluslongue=max(x)
print (phraselapluslongue)
n=0
while n<len(x):
    if x[n]==phraselapluslongue:
        print ("index : "+str(n)+"ième phrase")
        break
    else:
        n=n+1

print ('exiting now')
