#! /usr/bin/python3
# -*- coding:Utf8 -*-
#Écrivez une fonction qui échange les clés et les valeurs d’un dictionnaire (ce qui permettra par exemple de transformer un dictionnaire anglais/français en un dictionnaire français/anglais).
#On suppose que le dictionnaire ne contient pas plusieurs valeurs identiques
def changeKeyValue(dico):
    "Remplace toutes les clés par les valeurs du dico"
    dico2={}
    for key,value in dico.items():
        dico2[value]=key
    return dico2
if __name__ == "__main__":
    dicotest={'Vrai':'True','Faux':'False'}
    print(f"dictionnaire test: dicotest['Vrai']: {dicotest['Vrai']}")
    dico2=changeKeyValue(dicotest)
    print(f"dictionnaire inversé : dico2['True']: {dico2['True']}")
