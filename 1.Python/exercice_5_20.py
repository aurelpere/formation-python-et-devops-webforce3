#20. Définissez une fonction maximum(n1,n2,n3) qui renvoie le plus grand de 3 nombres n1, n2, n3 fournis en arguments. Par exemple, l’exécution de l’instruction :
#print(maximum(2,5,4)) doit donner le résultat : 5.

M=0
def maximum(n1,n2,n3):
    liste_=[n1,n2,n3]
    M=0
    def max_define(M,val):
        M=val
        return M
    def define(M,val):
        return M
    mdict={1:max_define,0:define}
    for index,val in enumerate(liste_):
        M=mdict[val>M](M,val)
    return M
print(maximum(2,5,4))
