#affichage des 20 premiers termes de la table de 7 avec * sur les multiples de 3
index=1
mult=7
result=0
while index<20:               
    result=index*mult    
    if result%3==0:
        print(result,'*')
    else:
        print(result)
    index+=1
