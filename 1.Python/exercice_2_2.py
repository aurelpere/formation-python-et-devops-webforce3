#2. Écrivez un programme qui affiche une table de conversion de sommes d’argent exprimées en euros, en dollars canadiens. La progression des sommes de la table sera « géométrique », comme dans l’exemple ci-dessous :
#1 euro(s) = 1.65 dollar(s) 2 euro(s) = 3.30 dollar(s) 4 euro(s) = 6.60 dollar(s) 8 euro(s) = 13.20 dollar(s) etc. (S’arrêter à 16384 euros.)

euro=1#on initialise la valeur euro à 1, cette variable va servir d'itérateur sur notre boucle while
while euro<=16384:#on fait une boucle while qui tourne tant que euro est inferieur ou égal à 16384 et s'arrête des que euro est superieur à cette valeur
    print ('euro:',euro,' ','dollar ca:',euro*1.65)# on imprime la chaine 'euro', la valeur de la variable euro, lachaine 'dollar ca', la valeur de la variable euro multipliée par 1.65. Les virgules servent à concaténer les résultats dans la fonction print
    euro=euro+1 # on itère: on affecte à euro la valeur de la variable euro à laquelle on ajoute 1
