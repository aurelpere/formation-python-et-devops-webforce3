#18.Écrire une boucle de programme qui demande à l’utilisateur d’entrer des
#notes d’élèves. La boucle se terminera seulement si l’utilisateur entre une
#valeur négative. Avec les notes ainsi entrées, construire progressivement une
#liste.
#Après chaque entrée d’une nouvelle note (et donc à chaque itération de la
#boucle), afficher le nombre de notes entrées, la note la plus élevée, la note la
#plus basse, la moyenne de toutes les notes.
#inport numpy as np
liste=[]
note=0
while note>=0:
    note=input('entrer une note d éleve')
    note=int(note)
    if note>=0:
        liste.append(note)
    print('nombre de notes',len(liste))
    print('note la plus élevée',max(liste))
    print('note la plus basse',min(liste))
    print('moyenne',sum(liste)/len(liste))
    
