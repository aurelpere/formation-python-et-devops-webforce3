#! /usr/bin/python3
# -*- coding:Utf-8 -*-
#script python table ascii
#Modifiez le script précédent pour explorer les codes situés entre 128 et 256, où vous
#retrouverez nos caractères accentués (parmi de nombreux autres). La relation numérique
#trouvée dans l’exercice précédent reste-t-elle valable pour les caractères accentués du
#français 
#print ("la correspondance entre unicode et ASCII ne semble pas être de mise dans python pour les nombre supérieurs à 128 en comparant avec des sources internet")
#print ('dans python : majuscules accentuées : 192 à 221 sans inclure 215')
#print ('dans python : minuscules accentuées : 224 à 253 en omettant ÿ qui correspond à 255')
#print ('relation numérique maj=min-32 semble valable pour les caractères accentués en omettant ÿ')

for i in range(32,257):
    print (i, " ", chr(i), end ="   ")

print (" \n")
print ("la correspondance entre unicode et ASCII ne semble pas être de mise dans python pour les nombre supérieurs à 128 en comparant avec des sources internet")
print ('dans python : majuscules accentuées : 192 à 221 sans inclure 215')
print ('dans python : minuscules accentuées : 224 à 253 en omettant ÿ')
print ('relation numérique maj=min-32 semble valable pour les caractères accentués en omettant ÿ')

