#! /usr/bin/python3
# -*- coding:Utf8 -*-
"""
this is test_dpaquets.py to test a python3, git, nano, telegram-destkop are installed and flask is installed
"""
import pytest
import subprocess
import testinfra
@pytest.fixture(scope='session')
def host(request):
    subprocess.check_call(['docker', 'build', '-t', 'myimage', './app'])  
    docker_id = subprocess.check_output(['docker', 'run', '-d', 'myimage','sleep','6000']).decode().strip()  
    yield testinfra.get_host("docker://" + docker_id)     
    subprocess.check_call(['docker', 'rm', '-f', docker_id])

@pytest.mark.parametrize("name,version", [
    ("python3", "3.9"),
    ("git", "2"),
    ("nano","5"),
    ("telegram-desktop","3")
])
def test_packages(host, name, version):
    "test name package is installed and can check version with commenting out"
    pkg = host.package(name)
    assert pkg.is_installed
    #assert pkg.version.startswith(version)

def test_pip_packages_installed(host):
    "test pip flask package is installed"
    assert host.pip("flask").is_installed
