#! /usr/bin/python3
# -*- coding:Utf8 -*-
"""
this is test_script.py to test that world is replace with webforce in hello.txt
"""
import os
from app.script import appli
def test_appli():
    with open('hello.txt','w',encoding='utf-8') as fileo:
        fileo.write('hello world')
    appli()
    with open('hello.txt','r',encoding='utf-8') as fileo:
        txt=fileo.read()
    assert 'hello webforce' in txt
    os.remove('hello.txt') 
