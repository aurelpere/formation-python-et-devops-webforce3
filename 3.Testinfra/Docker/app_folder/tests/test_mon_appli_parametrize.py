#!/usr/bin/python3
# coding: utf-8
"""
this is test_mon_appli.py with successful tests
"""

from app_folder.mon_appli import mafonction_carre
from app_folder.mon_appli import mafonction_cube
import pytest
@pytest.mark.parametrize("input_,expected", [("mafonction_carre(2)", 4), ("mafonction_carre(3)", 9), ("mafonction_carre(4)", 16),("mafonction_carre(5)", 24)])
def test_carre(input_,expected):
    "test function of mafonction_carre on a set of values"
    assert eval(input_) == expected
import pytest
@pytest.mark.parametrize("input_,expected", [("mafonction_cube(2)", 8), ("mafonction_cube(3)", 27), ("mafonction_cube(4)", 64)])
def test_cube(input_,expected):
    "test function of mafonction_cube"
    assert eval(input_) == expected
