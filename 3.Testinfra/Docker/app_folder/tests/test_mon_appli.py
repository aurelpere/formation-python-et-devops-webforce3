#!/usr/bin/python3
# coding: utf-8
"""
this is test_mon_appli.py with successful tests
"""
from app_folder.mon_appli import mafonction_carre
from app_folder.mon_appli import mafonction_cube
def test_mafonction_carre():
    "test function of mafonction_carre"
    assert mafonction_carre(2) == 4
def test_mafonction_cube():
    "test function of mafonction_cube"
    assert mafonction_cube(2) == 8
