#!/usr/bin/python3
# coding: utf-8
"""
this is test_mon_appli_wrong.py with failing tests
"""
from app_folder.mon_appli import mafonction_carre
from app_folder.mon_appli import mafonction_cube
def test_mafonction_carre():
    assert mafonction_carre(2) == 5
def test_mafonction_cube():
    assert mafonction_cube(2) == 6
