#! /usr/bin/python3
# -*- coding:Utf8 -*-
"""
this is test_dpaquets.py to test a python3, git, nano, telegram-destkop are installed and flask is installed
"""
def test_packages_installed(host):
    "test python3,git,nano,telegram-desktop packages"
    assert host.package("python3").is_installed
    assert host.package("git").is_installed
    assert host.package("nano").is_installed
    assert host.package("telegram-desktop").is_installed

def test_pip_packages_installed(host):
    "test pip flask package is installed"
    assert host.pip("flask").is_installed
