#! /usr/bin/python3
# -*- coding:Utf8 -*-
"""
this is test_docker_container_running.py to test a container is running or not and docker service is active and enabled
"""

def test_dockeractive(host):
    "test docker service is running and enabled"
    docker = host.service("docker")
    assert docker.is_running
    assert docker.is_enabled

def test_docker_running(host):
    "test container_name container is running"
    mon_hello_docker = host.docker("container_name")
    assert mon_hello_docker.is_running

