#Corrige exercice testinfra docker 

## lancer le container de l'exercice précédent (CorrigeExercice_Dockerfile.zip)

docker build . -f CorrigeExercice_Dockerfile -t img_name && docker run -d --name container_name img_name:latest sleep 70000

## lancer le test sur le host pour verifier que le service docker est actif et enabled et que docker tourne:
pip install -r requirements.txt
pytest test_docker_container_running.py 
find . -type d -name "__pycache__" -exec rm -rf {} +

>vous vous rendez compte qu'il est important de nommer les containers dans des scripts ou fichier puisque le test a besoin du nom du container pour verifier qu'il tourne 

## lancer le test dans le container pour verifier que les paquets "python3, git,nano et telegram-desktop" sont bien installés et que la bibliothèque python "flask" est bien installé

docker cp test_paquets.py container_name:/app/test_paquets.py
docker exec container_name pip install pytest pytest-testinfra
docker exec container_name pytest test_paquets.py
