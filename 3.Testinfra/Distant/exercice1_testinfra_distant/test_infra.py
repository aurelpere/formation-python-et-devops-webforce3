#!/usr/bin/python3
# coding: utf-8
"""
this is test_infra.py
"""
from tzlocal import get_localzone

def test_timezone():
    "test timezone is Europe/Paris"
    result = get_localzone()
    assert result._key == 'Europe/Paris'


def test_packages_installed(host):
    "test packages are installed"
    assert host.package("git").is_installed
    assert host.package("curl").is_installed
    assert host.package("ufw").is_installed
    assert host.package("docker-ce").is_installed
    assert host.package("docker-ce-cli").is_installed
    assert host.package("docker-compose").is_installed
    assert host.package("rsync").is_installed
    assert host.package("nano").is_installed
    assert host.package("python3").is_installed
    assert host.package("python3-pip").is_installed


def test_dockeractive(host):
    "test docker service is running and enabled"
    docker = host.service("docker")
    assert docker.is_running
    assert docker.is_enabled

def test_ufw(host):
    "test ufw service is running and enabled"
    ufw = host.service("ufw")
    assert ufw.is_running
    assert ufw.is_enabled


def test_sshdconfig(host):
    "test sshd_config file is appropriate"
    sshdconfig = host.file("/etc/ssh/sshd_config")
    assert sshdconfig.contains('Port 4444') 
    assert sshdconfig.contains('Protocol 2')
    assert sshdconfig.contains('HostKey /etc/ssh/ssh_host_rsa_key')
    assert sshdconfig.contains('HostKey /etc/ssh/ssh_host_dsa_key')
    assert sshdconfig.contains('HostKey /etc/ssh/ssh_host_ecdsa_key')
    assert sshdconfig.contains('HostKey /etc/ssh/ssh_host_ed25519_key')
    assert sshdconfig.contains('UsePrivilegeSeparation yes')
    assert sshdconfig.contains('KeyRegenerationInterval 3600')
    assert sshdconfig.contains('ServerKeyBits 1024')
    assert sshdconfig.contains('SyslogFacility AUTH')
    assert sshdconfig.contains('LogLevel INFO')
    assert sshdconfig.contains('LoginGraceTime 120')
    assert sshdconfig.contains('PermitRootLogin no')
    assert sshdconfig.contains('StrictModes yes')
    assert sshdconfig.contains('RSAAuthentication yes')
    assert sshdconfig.contains('PubkeyAuthentication yes')
    assert sshdconfig.contains('IgnoreRhosts yes')
    assert sshdconfig.contains('RhostsRSAAuthentication no')
    assert sshdconfig.contains('HostbasedAuthentication no')
    assert sshdconfig.contains('PermitEmptyPasswords no')
    assert sshdconfig.contains('ChallengeResponseAuthentication no')
    assert sshdconfig.contains('X11Forwarding no')
    assert sshdconfig.contains('X11DisplayOffset 10')
    assert sshdconfig.contains('PrintMotd no')
    assert sshdconfig.contains('TCPKeepAlive yes')
    assert sshdconfig.contains('PrintLastLog yes')
    assert sshdconfig.contains('AcceptEnv LANG LC_*')
    assert sshdconfig.contains('Subsystem sftp /usr/lib/openssh/sftp-server')
    assert sshdconfig.contains('UsePAM yes')
    assert sshdconfig.contains('UseDNS no')
    assert sshdconfig.contains('AllowUsers user')
    assert sshdconfig.contains('PasswordAuthentication no')
    assert sshdconfig.contains('AllowGroups user')
    assert sshdconfig.contains('GSSAPIAuthentication no')


def test_ufwports(host):
    "test ufw ports"
    with host.sudo():
        out=host.check_output("ufw status")
        assert '4000:4010/tcp              ALLOW       Anywhere' in out
        assert '4000:4010/udp              ALLOW       Anywhere' in out
        assert '4444                       ALLOW       Anywhere' in out
        assert '80                         ALLOW       Anywhere' in out
        assert '443                        ALLOW       Anywhere' in out
        assert '10051                      ALLOW       Anywhere' in out
        assert '10050                      ALLOW       Anywhere' in out


def test_docker_running(host):
    "test container_name container is running"
    mon_hello_docker = host.docker("container_name")
    assert mon_hello_docker.is_running

