#!/bin/bash
sudo tee -a /etc/ssh/sshd_config <<EOF
Port 4444
Protocol 2
HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_dsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key
UsePrivilegeSeparation yes
KeyRegenerationInterval 3600
ServerKeyBits 1024
SyslogFacility AUTH
LogLevel INFO
LoginGraceTime 120
PermitRootLogin no
StrictModes yes
RSAAuthentication yes
PubkeyAuthentication yes
IgnoreRhosts yes
RhostsRSAAuthentication no
HostbasedAuthentication no
PermitEmptyPasswords no
ChallengeResponseAuthentication no
X11Forwarding yes
X11DisplayOffset 10
PrintMotd no
PrintLastLog yes
TCPKeepAlive yes
AcceptEnv LANG LC_*
Subsystem sftp /usr/lib/openssh/sftp-server
UsePAM yes
AllowUsers your_user
PasswordAuthentication no
EOF
sudo apt update && sudo apt install curl nano git ufw rsync python3 build-essential
sudo timedatectl set-timezone Europe/Paris
sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw allow 4444
sudo ufw allow 80
sudo ufw allow 443
sudo ufw allow 8080
sudo ufw allow 5000
sudo ufw allow 10050
sudo ufw allow 10051
sudo systemctl restart sshd
