#! /usr/bin/python3
# -*- coding:Utf8 -*-
import sys
import paramiko 
from scp import SCPClient

#Connect function
def createSSHClient(server="your_server", port=22, key="path_to_your_key", user="your_user",password=""):
    "createSSHClient return SSHClient object from paramiko.client class and connect to server on port using user and key"   
    ssh = paramiko.client.SSHClient()
    ssh.load_system_host_keys(key)
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy()) #to avoid raise SSHException Server XY not found in known_hosts
    ssh.connect(hostname=server,port=port,username=user, key_filename=key)
    return ssh

#Connecting
ssh=createSSHClient()

# Define progress callback that prints the current percentage completed for the file
def progress(filename, size, sent):
    "create progress status callback"
    sys.stdout.write("%s's progress: %.2f%%   \r" % (filename, float(sent)/float(size)*100) )

# SCPCLient takes a paramiko transport and progress callback as its arguments.
scp = SCPClient(ssh.get_transport(),progress=progress)

#use scp with python: Uploading the 'corrige_exercice1_docker_compose' directory with its content in the
# /tmp remote directory
scp.put('corrige_exercice1_docker_compose', recursive=True, remote_path='/tmp')
scp.close()


for cmd in ["sudo apt update",
            "sudo apt install docker.io",
            "sudo apt install docker-compose",
            "cd /tmp/corrige_exercice1_docker_compose && sudo docker-compose up -d",
            "echo 'done'"]:
    stdin, stdout, stderr = ssh.exec_command(cmd)
    std_list=stdout.readlines()+stderr.readlines()
    for txt in std_list:    
        print(txt)
#to replace set -e
    return_code = stdout.channel.recv_exit_status()
    if return_code > 0:
        print(f"Command '{cmd}' was not successful")
        break

ssh.close()

