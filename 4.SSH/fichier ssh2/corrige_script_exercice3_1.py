#! /usr/bin/python3
# -*- coding:Utf8 -*-
import sys
import paramiko 
from scp import SCPClient

#Connect function
def createSSHClientserver="your_server", port=22, key="path_to_your_key", user="your_user",password=""):
    "createSSHClient return SSHClient object from paramiko.client class and connect to server on port using user and key"   
    ssh = paramiko.client.SSHClient()
    ssh.load_system_host_keys(filename=key)
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy()) #to avoid raise SSHException Server XY not found in known_hosts
    ssh.connect(hostname=server,port=port,username=user,key_filename=key)
    return ssh

#Connecting
ssh=createSSHClient()

for cmd in ["sudo fallocate -l 4G /swapfile ",
            "sudo chmod 600 /swapfile",
            "sudo mkswap /swapfile",
            "sudo swapon /swapfile",
            "sudo swapon --show",
            "sudo cp /etc/fstab /etc/fstab.bak",
            "echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab"]:
    stdin, stdout, stderr = ssh.exec_command(cmd)
    std_list=stdout.readlines()+stderr.readlines()
    for txt in std_list:    
        print(txt)
#to replace set -e
    return_code = stdout.channel.recv_exit_status()
    if return_code > 0:
        print(f"Command '{cmd}' was not successful")
        break
ssh.close()

