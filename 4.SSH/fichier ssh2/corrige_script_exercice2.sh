#!/bin/bash
eval $(ssh-agent)
ssh-add mykey.pem
scp -rC corrige_exercice1_docker_compose admin@myserver:/tmp
ssh admin@myserver << 'ENDSSH'
set -ex #halt on error and print all executed commands
sudo apt update
sudo apt install docker.io
sudo apt install docker-compose
cd /tmp/corrige_exercice1_docker_compose
sudo docker-compose up -d
ENDSSH


