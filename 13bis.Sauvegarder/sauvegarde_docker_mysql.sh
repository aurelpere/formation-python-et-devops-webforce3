#!/bin/bash

# DB Container Backup Script Template
# ---
# This backup script can be used to automatically backup databases in docker containers.
# It currently supports mysql containers.
# 

DAYS=2
echo "number of days to keep backups : $DAYS"
BACKUPDIR=/home/xcad/backup
echo "backup directory : $BACKUPDIR"

# backup all mysql/mariadb containers

#cut -d sets delimiter
#cut -f1 sets selection to fields 1
CONTAINER=$(docker ps --format '{{.Names}}:{{.Image}}' | grep 'mysql' | cut -d":" -f1)
echo $CONTAINER

#if directory $BACKUPDIR doesnt exist, create directory
if [ ! -d $BACKUPDIR ]; then
    mkdir -p $BACKUPDIR
fi


for i in $CONTAINER; do
    MYSQL_DATABASE=$(docker exec $i env | grep MYSQL_DATABASE |cut -d"=" -f2)
    MYSQL_PWD=$(docker exec $i env | grep MYSQL_ROOT_PASSWORD |cut -d"=" -f2)

    docker exec -e MYSQL_DATABASE=$MYSQL_DATABASE -e MYSQL_PWD=$MYSQL_PWD \
        $i /usr/bin/mysqldump -u root $MYSQL_DATABASE \
        | gzip > $BACKUPDIR/$i-$MYSQL_DATABASE-$(date +"%Y%m%d%H%M").sql.gz
    #wc prints newline, word, and byte counts for each file
    OLD_BACKUPS=$(ls -1 $BACKUPDIR/$i*.gz |wc -l)
    # -gt means greater than : if the number of lines is greater than the DAYS variable, then
    if [ $OLD_BACKUPS -gt $DAYS ]; then
        #find backup files older than DAYS number of days and delete them
        find $BACKUPDIR -name "$i*.gz" -daystart -mtime +$DAYS -delete
    fi
done


